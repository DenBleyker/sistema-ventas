<?php

use Illuminate\Database\Seeder;

use App\Categoria;
class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria = new Categoria;
        $categoria->nombre = 'Juguetes';
        $categoria->descripcion = 'Juguetes para niño y niña';
        $categoria->save();
    }
}
