<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('contenido/contenido');
});

Route::get('/categoria','CategoriaController@mostrar');
Route::post('/categoria/registrar','CategoriaController@registrar');
Route::put('/categoria/actualizar','CategoriaController@actualizar');
Route::put('/categoria/activo','CategoriaController@activo');
Route::get('/categoria/buscar/', 'CategoriaController@buscar');
