<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\DB;
class CategoriaController extends Controller
{
    /**
     * Muestra una lista de las categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function mostrar()
    {
        $categorias = DB::table('categorias')
        ->orderBy('activo','desc')
        ->get();
        return $categorias;
    }

    public function buscar(Request $request){
        $buscar = $request->input("buscar");
        $criterio = $request->input("criterio");
        if($criterio == 'nombre' && $buscar != ''){
            $categorias = DB::table('categorias')
            ->where('nombre','like',"$buscar%")
            ->get();
        }else if($criterio == 'descripcion' && $buscar !=''){
            $categorias = DB::table('categorias')
            ->where('descripcion','like',"%$buscar%")
            ->get();
        }else{
            $categorias = DB::table('categorias')
            ->orderBy('activo','desc')
            ->get();
        }
        return $categorias;
    }
    /**
     * Crea una nueva categoria.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registrar(Request $request)
    {
        //
      try {
        $categoria = new Categoria;
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->save();
      } catch (\Throwable $th) {

          throw $th;
      }
    }

    /**
     * Actualiza una categoria.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        $categoria = Categoria::findOrFail($request->id);
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->activo = true;
        $categoria->save();

    }

    /**
     * Cambia el estado de una categoria.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activo(Request $request)
    {
        //
        $categoria = Categoria::findOrFail($request->id);
        $categoria->activo = !$request->activo;
        $categoria->save();
    }
}
